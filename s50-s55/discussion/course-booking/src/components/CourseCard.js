import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import{Link} from 'react-router-dom'

import {useContext} from 'react';
import UserContext from '../UserContext.js'

// import the useState hook from react
import {useState, useEffect} from 'react'

export default function CourseCard(props){
	//consume the content of the UserContext
	const {user} = useContext(UserContext)

	// console.log(props.courseProp);

	// object destructuring
	const {_id, name, description, price} = props.courseProp;

	// Use the state hook for this component to be able to store its state
	// States are use to keep track of information related to individual components

		// Syntax : const [getter, setter] = useState(initialGetterValue)
	const [count, setCount] = useState(0)
	const [seat, setSeat] = useState(30)
	const [isDisabled, setIsDisabled] = useState(false)

	// setcount(2);
	// console.log(count)

	// this function will be invoke when Enroll button is click

	function enroll(){
		// setCount(count + 1);
		// console.log(count):
		if(seat === 0){
			return(
				alert('no more seats!')
				)
		}else{
			setCount(count + 1)
			setSeat(seat - 1)
			return(seat)
		};
	}

	// the function or the side effect in our useEffect hook will invoke or run on the initial loading of our application and when there is/are change/changes on out dependencies

	// Syntax:
		// useEffect(side effect, [dependencies])
	useEffect(() =>{
		// console.log(`Hi I'm from the useEffect`)
		if(seat === 0){
			setIsDisabled(true)
		}
	}, [seat])

	// console.log(id);
	return (
		<Container className = 'mt-3'>
			<Row>
				<Col>
					<Card>
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>
					        {description}
					        </Card.Text>

					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>
					        {price}
					        </Card.Text>

					        <Card.Subtitle>Enrollees</Card.Subtitle>
					        <Card.Text>
					        {seat}
					        </Card.Text>
					        {
					        	user !==null
					        	?
					        	<Button as = {Link} to = {`/courses/${_id}`} variant="primary">Details</Button>
					        	:
					        	<Button as = {Link} to = '/login' >Log in to enroll!</Button>
					        }

					      </Card.Body>
					    </Card>
				</Col>
			</Row>
		</Container>

		)


}
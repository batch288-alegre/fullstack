import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavBar from './components/AppNavBar.js';
import Courses from './pages/Courses.js';
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js'
// The BrowserRouter component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
// The Routes component holds all our Route components. It select which 'Route' component to show based on the url endpoint.
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import CourseView from './pages/CourseView.js'
import {useState, useEffect} from 'react';

// import UserProvider
import {UserProvider} from './UserContext.js';


  function App() {
    
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {

    localStorage.clear()

  }

  // useEffect(() => {
  //   console.log(user);
  //   console.log(localStorage.getItem('token'));
  // },[user])

  useEffect(() => {
    if(localStorage.getItem('token')){

      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(result => result.json())
      .then(data => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      })
    }

  },[])

  return (

  <UserProvider value = {{user, setUser, unsetUser}}>
    <BrowserRouter>
        <Routes>

        <Route path = '/register' element = {
          user.id !==null
          ?
          <PageNotFound/>

          :
          <>
          <AppNavBar/>
          <Register/>
          </>

          
        } />

        <Route path = '/login' element = {
          user.id !== null
          ?
          <PageNotFound/>

          :
          <>
          <AppNavBar/>
          <Login/>
          </>

        } />

          <Route path = '/' element = {
              <>
                <AppNavBar/>
                <Home/>
              </>
          }/>

          <Route path = '/courses' element = {
              <>
                <AppNavBar/>
                <Courses/>
              </>
          } />


          <Route path = '/logout' element = {
            <>
              <AppNavBar/>
              <Logout/>
            </>
          } />

          <Route path = "/courses/:courseId" element = {
            <>
              <AppNavBar/>
              <CourseView/>
            </>
          } />
          <Route path = "*" element = {<PageNotFound/>} />

        </Routes>
    </BrowserRouter>
  </UserProvider>

   );
}

export default App;

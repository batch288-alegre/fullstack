import Banner from '../components/Banner.js';
import Higlights from '../components/Highlights.js';
import CourseCard from '../components/CourseCard.js'

export default function Home() {

	return(
		<>
			<Banner />
			<Higlights />
		</>
		)
}
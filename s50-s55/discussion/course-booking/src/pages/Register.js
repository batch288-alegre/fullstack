import {Button, Col, Container, Form, Row} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom'
import Swal2 from 'sweetalert2'

import UserContext from '../UserContext.js'


export default function Register(){
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState ('')
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isDisabled, setIsDisabled] = useState(true)

	// we consume the setUser function from the UserContext
	const {setUser} = useContext(UserContext); 

	// we contain useNavigate to navigate variagble
	const navigate = useNavigate();
	// We have to use the useEffect in enabling the submit button
	useEffect(() => {
		if(firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '' && password1 === password2 && mobileNo.length > 10 && password1.length > 6){

			setIsDisabled(false);

		} else {

			setIsDisabled(true);

		}
	}, [firstName, lastName, email, mobileNo, password1, password2])

	// function that wil be triggered once we submit
	function register(event){

		event.preventDefault()

		// localStorage.setItem('email', email);
		// setUser(localStorage.getItem('email'))

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNo: mobileNo
			})

		})
		.then(response => response.json())
		.then(data => {
			if(data){
				Swal2.fire({
					title: 'Successfully registered!',
					icon: 'success',
					text: 'Thank you for registering'
				})
				navigate('/login')
			}else{
				Swal2.fire({
				    title: 'Something went wrong',
				    icon: 'error',
				    text: 'Please try again.'
				})
			}
		})
		
		// clear inputs
		// setEmail('')
		// setPassword1('')
		// setPassword2('')
	}
return(
	<Container className = "mt-5">
		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Register</h1>

				<Form onSubmit = {event => register(event)}>
				    <Form.Group className="mb-3" controlId="formBasicFirstName">
				        <Form.Label>FirstName</Form.Label>
				        <Form.Control 
				        	type="firstName"
				        	value = {firstName} 
				        	onChange = {event => {
				        	
				        		setFirstName(event.target.value)
				        		console.log(firstName)
				        	}}
				        	placeholder="Enter your First Name" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicLastName">
				        <Form.Label>LastName</Form.Label>
				        <Form.Control 
				        	type="lastName"
				        	value = {lastName} 
				        	onChange = {event => {
				        	
				        		setLastName(event.target.value)
				        		console.log(lastName)
				        	}}
				        	placeholder="Enter your Last Name" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email"
				        	value = {email} 
				        	onChange = {event => {
				        	
				        		setEmail(event.target.value)
				        		console.log(email)
				        	}}
				        	placeholder="Enter email" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicMobileNo">
				        <Form.Label>Mobile Number</Form.Label>
				        <Form.Control 
				        	type="mobileNo"
				        	value = {mobileNo} 
				        	onChange = {event => {
				        	
				        		setMobileNo(event.target.value)
				        		console.log(mobileNo)
				        	}}
				        	placeholder="Mobile Number" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password1} 
				        	onChange = {event => {
				        	
				        		setPassword1(event.target.value)
				        		console.log(password1)
				        	}}
				        	placeholder="Password" />
				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password2}
				        	onChange = {event => {
				        	
				        		setPassword2(event.target.value)
				        		console.log(password2)
				        	}} 
				        	placeholder="Confirm Password" />
				    </Form.Group>

				    <p>Have an account already ? <Link to = "/login">Log in here</Link></p>
				    <Button variant="primary" type="submit" disabled = {isDisabled}>Submit</Button>
				</Form>
			</Col>
		</Row>
	</Container>
	)
}
